console.log("Hello Thursday");
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of Addition Operator " + sum);

let difference = x - y;
console.log("Result of Subtraction Operator " + difference);

let product = x * y;
console.log("Result of Multiplication Operator " + product);

let quotient = x / y;
console.log("Result of Division Operator " + quotient);

let modulo = x % y;
console.log("Result of Modulo Operator " + modulo);

let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition Assignment Operator is: "+ assignmentNumber);

assignmentNumber += 2;
console.log("Result of addition Assignment Operator is: "+ assignmentNumber);

assignmentNumber -= 2;
console.log("Result of Subtraction Assignment Operator is: "+ assignmentNumber);

assignmentNumber *= 2;
console.log("Result of Multiplication Assignment Operator is: "+ assignmentNumber);

assignmentNumber /= 2;
console.log("Result of Division Assignment Operator is: "+ assignmentNumber);

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of MDAS Assignment Operator is: " + mdas); 

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of PEMDAS Assignment Operator is: " + pemdas); 

let z = 1;

let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

let num1 = '10';
let num2 = 12;

let coercion = num1 + num2;
console.log(coercion);
console.log(typeof coercion);

let num3 = 16;
let num4 = 14;

let noneCoercion = num3 + num4;
console.log(noneCoercion);
console.log(typeof noneCoercion);

let num5 = true + 1;
console.log(num5);
console.log(typeof num5);

let num6 = false + 1;
console.log(num6);
console.log(typeof num6);

let juan = 'juan';

console.log(" ");
console.log(1==1);
console.log(1==2);
console.log(1=='1');
console.log(0==false);
console.log("juan" == 'juan');
console.log('juan' == juan);

// Inequality Operator (!=)
console.log(" ");

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log("juan" != 'juan');
console.log('juan' != juan);

console.log(" ");

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log("juan" === 'juan');
console.log('juan' === juan);

console.log(" ");

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log("juan" !== 'juan');
console.log('juan' !== juan);

// Relational operators 

let a = 50;
let b = 65;

let isGreaterThan = a > b;
let isLessThan = a < b;
let isGTorEqual = a >= b;
let isLTorEqual = a <= b;

console.log(" ");

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

//Logical Operator 

let isLegalAge = true;
let isRegistered = false;

let allRequirements = isLegalAge && isRegistered;
console.log("Result of Logical AND Operator " + );

let someRequirements = isLegalAge || isRegistered;
console.log("Result of Logical OR Operator ");
 
let someRequirementsNotMet = !isRegistered;
console.log("Result of Logical NOT Operator ");